# Deser

Simple JSON deserializer for ReScript.

**Note**: In Rescript 12, this will be kind of useless.  So this will be
likely be the last version series of Deser.

**Important**: Version 6+ changed the name of the module from `JSON` to
`Deser` to avoid clashes with `RescriptCore.JSON`.

You may want to watch the tests live here: https://kaiko-systems.gitlab.io/rescript-deser/

## Description

This module allows you to create deserialize structures from JSON into an
internal type-safe type in ReScript.  You don't (shouldn't) have to trust the
data you're getting from an API will always fit the expected shape and types.
So you might need to create a "parser" so that you can be sure the data is
type-correct.

## Usage

You need to create a description of your data using ``Deser.Field.t``.

```rescript
module MyData = {
    @deriving(jsConverter)
    type tag = [#a | #b | #c]

    type t = {
        id: string,
        tags: array<tag>
    }
}

module MyDataDeserializer = Deser.MakeDeserializer({
    type t = MyData.t
    open Deser.Field

    let fields = Object([
        ("id", String),
        ("tags", Array(variadicString(MyData.tagFromJs)))
    ])
})
```

The resultant module has type:

```rescript
module type Deserializer = {
  type t
  let name: string
  let fromJSON: Js.Json.t => result<t, string>
}
```

The input module must have the module type:

```rescript
module type Serializable = {
  type t
  let fields: Field.t
}
```


### List of variants for `Field.t`.

- `String`, parses a JSON string into a ReScript `string`.

- `Literal(expected)`, parses a JSON string that must match the expected value.

- `Int`, parses a JSON number into a ReScript `int`

- `Float`, parses a JSON number into a ReScript `float`

- `Boolean`

- `Any`, *unsafe type* to allow anything.  The result is the same underlying
  JS representation of the object that comes from the JSON data.

- `Date`, parses either a string representation of a date (datetime) or a
  floating point representation of date (datetime) into `Js.Date.t`; we make
  sure the result is valid and won't return NaN afterwards.

  This basically calls, `Js.Date.fromString` or `Js.Date.fromFloat`; and tests
  the resulting value.

  `Datetime` is an alias for `Date`.

- `Optional(Field.t)`, allow the field to be missing from the JSON data in
  which case, return `None`; if the field is present and valid return
  `Some(value)`.

- `OptionalWithDefault(Field.t, FieldValue.t)`; same as optional but return a
  default value instead.  Notice, this is unsafe if the actual type of the
  default value does not match the expected type.

- `Tuple(array<Field.t>)`, parses a JSON list with an exact number of items of
  varying types.

- `Object(array<(string, Field.t)>)`, parses a JSON object that should have
  exactly the fields described; missing items are not allowed unless they are
  `Optional`, `OptionalWithDefault`.

- `Mapping(Field.t)`, parses a JSON object with unknown keys (of type string)
  and a given type of value.  Valid values have the internal type
  `Prelude.Dict.t`.

- `Deserializer(module(Deserializer))`, parses an JSON object with the function
  `fromJSON` of another deserializer.  This allows the composition of
  deserializers.

- `Collection(module(Deserializer))`, parses a list of items with the function
  `fromJSON` of another deserializer.  Invalid items are ignored.

  This is basically a shortcut to
  `Array(DefaultWhenInvalid(Optional(Deserializer(module(M))),
  FieldValue.null))`, with the additional post-processing to remove `None`
  values.

- `DefaultWhenInvalid(Field.t, FieldValue.t)`, if the JSON contains an invalid
  value, use a default instead.

- `Array(Field.t)`, parses a JSON list with homogeneous items.


## Current limitations with recursive data

Currently we cannot deal with recursive data types.  A type like

```rescript
type rec Node<'t> = Leave('t) | (Branch(array<Node<'t>>)
```

Cannot be automatically deserialized.


## License

The MIT License

Copyright © 2022 Kaiko Systems GmbH and Collaborators

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the “Software”), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
