open QUnit
open RescriptCore

module Appointment = {
  type t = {
    note: string,
    date: Js.Date.t,
    extra: option<string>,
  }
  module Deserializer = Deser.MakeDeserializer({
    type t = t
    open Deser.Field

    let fields = Object([("note", String), ("date", Date), ("extra", Optional(String))])
  })
}

module_("Basic deserializer", _ => {
  let valid: array<(_, Appointment.t)> = [
    (
      %raw(`{"note": "Bicentennial doctor's appointment", "date": "2100-01-01"}`),
      {
        note: "Bicentennial doctor's appointment",
        date: Js.Date.fromString("2100-01-01"),
        extra: None,
      },
    ),
    (
      %raw(`{
      "note": "Bicentennial doctor's appointment",
      "date": "2100-01-01",
      "extra": "Don't take your stop-aging pills the night before the appointment",
    }`),
      {
        note: "Bicentennial doctor's appointment",
        date: Js.Date.fromString("2100-01-01"),
        extra: Some("Don't take your stop-aging pills the night before the appointment"),
      },
    ),
  ]

  test("Correctly deserializes data", qunit => {
    qunit->expect(valid->Array.length)
    valid->Array.forEach(
      ((data, expected)) => {
        Js.Console.log2("Running sample", data)
        switch Appointment.Deserializer.fromJSON(data) {
        | Ok(result) => qunit->deepEqual(result, expected, "result == expected")
        | Error(msg) => Js.Console.error(msg)
        }
      },
    )
  })

  let invalid = [
    (
      "Missing non-optional field",
      %raw(`{"extra": "Bicentennial doctor's appointment", "date": "2100-01-01"}`),
    ),
  ]

  test("Correctly catches invalid data", qunit => {
    qunit->expect(invalid->Array.length)
    invalid->Array.forEach(
      ((message, data)) => {
        Js.Console.log3("Running sample", message, data)
        switch Appointment.Deserializer.fromJSON(data) {
        | Ok(result) => Js.Console.error2("Invalid being accepted: ", result)
        | Error(msg) => {
            Js.Console.log2("Correctly detected:", msg)
            qunit->ok(true, true)
          }
        }
      },
    )
  })
})

module_("Recursive deserializer", _ => {
  module TrivialTree = {
    type rec t<'a> = {
      data: 'a,
      children: array<t<'a>>,
    }

    module DeserializerImpl = Deser.MakeDeserializer({
      type payload
      type rec t = {
        data: payload,
        children: array<t>,
      }
      open Deser.Field

      let fields = Object([("data", Any), ("children", Array(Self))])
    })

    module Deserializer = {
      include DeserializerImpl

      let fromJSON = data => data->DeserializerImpl.fromJSON->Result.map(x => x->Obj.magic)
    }
  }

  let valid: array<(_, TrivialTree.t<string>)> = [
    (
      %raw(`{"data": "A", "children": [{"data": "A1", "children": []}, {"data": "B", "children": [{"data": "C", "children": []}, {"data": "D", "children": []}]}]}`),
      {
        data: "A",
        children: [
          {data: "A1", children: []},
          {data: "B", children: [{data: "C", children: []}, {data: "D", children: []}]},
        ],
      },
    ),
  ]

  test("Trivial recursion detection: Ok", qunit => {
    qunit->expect(1)
    qunit->deepEqual(TrivialTree.Deserializer.checkFieldsSanity(), Ok(), "Ok")
  })

  test("Infinite list", qunit => {
    module InfiniteList = Deser.MakeDeserializer({
      open Deser.Field

      type t
      let fields = Object([("head", String), ("tail", Self)])
    })

    qunit->expect(1)
    qunit->deepEqual(InfiniteList.checkFieldsSanity()->Result.isError, true, "Ok")
  })

  test("Finite list", qunit => {
    module List = Deser.MakeDeserializer({
      open Deser.Field

      type t
      let fields = Object([("head", String), ("tail", Optional(Self))])
    })

    qunit->expect(1)
    qunit->deepEqual(List.checkFieldsSanity(), Ok(), "Ok")
  })

  test("Correctly deserializes recursive data", qunit => {
    qunit->expect(valid->Array.length)
    valid->Array.forEach(
      ((data, expected)) => {
        Js.Console.log2("Running sample", data)
        switch TrivialTree.Deserializer.fromJSON(data) {
        | Ok(result) => qunit->deepEqual(result, expected, "result == expected")
        | Error(msg) => Js.Console.error(msg)
        }
      },
    )
  })

  test("Recursion in sub-deserializer", qunit => {
    module List = Deser.MakeDeserializer({
      open Deser.Field

      type t
      let fields = Object([("head", String), ("tail", Optional(Self))])
    })

    module Ledger = Deser.MakeDeserializer({
      open Deser.Field

      type t
      let fields = Object([("records", Deserializer(module(List))), ("next", Optional(Self))])
    })

    let data = %raw(`
       {"records": {"head": "A", "tail": {"head": "B"}},
        "next": {"records": {"head": "A", "tail": {"head": "B"}}}}
    `)
    let expected = {
      "records": {"head": "A", "tail": {"head": "B", "tail": None}},
      "next": {
        "records": {"head": "A", "tail": {"head": "B", "tail": None}},
        "next": None,
      },
    }

    qunit->expect(1)
    qunit->deepEqual(data->Ledger.fromJSON->Obj.magic, Ok(expected), "nice ledger")
  })
})
