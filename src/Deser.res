open RescriptCore

module FieldValue = {
  type t
  external string: string => t = "%identity"
  external int: int => t = "%identity"
  external float: float => t = "%identity"
  external boolean: bool => t = "%identity"
  external array: array<t> => t = "%identity"
  external object: Dict.t<t> => t = "%identity"
  external mapping: Dict.t<t> => t = "%identity"
  external any: 'a => t = "%identity"
  @val external null: t = "undefined"

  external asString: t => string = "%identity"
  external asInt: t => int = "%identity"
  external asFloat: t => float = "%identity"
  external asBoolean: t => bool = "%identity"
  external asArray: t => array<'a> = "%identity"
  external asObject: t => 'a = "%identity"
}

exception TypeError(string)

@doc("The module type of a built deserializer which is suitable to add as a subparser.")
module type Deserializer = {
  type t
  let name: string
  let fromJSON: Js.Json.t => result<t, string>
  let checkFieldsSanity: unit => result<unit, string>
}

module Field = {
  type rec t =
    | Any
    | String
    | Literal(string)
    | Int
    | Float
    | Boolean
    | Array(t)
    /// These SHOULD strings in ISO format, but we only validate the string
    /// can be represented in Js.Date without spewing NaN all over the place;
    /// Js.Date.fromString("xxx") returns an object that is mostly unusable.
    ///
    /// We also allow floats and then use Js.Date.fromFloat.
    | Date
    | Datetime // alias of Date

    | Tuple(array<t>)
    | Object(array<(string, t)>)
    | Optional(t)
    | OptionalWithDefault(t, FieldValue.t)
    /// An arbitrary mapping from names to other arbitrary fields.  The
    /// difference with Object, is that you don't know the names of the
    /// expected entries.
    | Mapping(t)

    | Deserializer(module(Deserializer))

    /// A specialized Array of deserialized items that ignores unparsable
    /// items and returns the valid collection.  This saves the user from
    /// writing 'Array(DefaultWhenInvalid(Optional(Deserializer(module(M)))))'
    /// and then post-process the list of items with 'Array.keepSome'
    | Collection(module(Deserializer))
    | DefaultWhenInvalid(t, FieldValue.t)

    // FIXME: this is used to add additional restrictions like variadictInt or
    // variadicString; but I find it too type-unsafe.  I might consider having
    // a Constraints for this in the future.
    | Morphism(t, FieldValue.t => FieldValue.t)

    | Self

  let usingString = (f: string => 'a) => value => value->FieldValue.asString->f->FieldValue.any
  let usingInt = (f: int => 'a) => value => value->FieldValue.asInt->f->FieldValue.any
  let usingFloat = (f: float => 'a) => value => value->FieldValue.asFloat->f->FieldValue.any
  let usingBoolean = (f: bool => 'a) => value => value->FieldValue.asBoolean->f->FieldValue.any
  let usingArray = (f: array<'a> => 'b) => value => value->FieldValue.asArray->f->FieldValue.any
  let usingObject = (f: 'a => 'b) => value => value->FieldValue.asObject->f->FieldValue.any

  let variadicInt = (hint: string, fromJs: int => option<'variadicType>) => Morphism(
    Int,
    usingInt(i => {
      switch i->fromJs {
      | Some(internalValue) => internalValue
      | None =>
        raise(TypeError(`This Int(${i->Int.toString}) not a valid value here. Hint: ${hint}`))
      }
    }),
  )
  let variadicString = (hint: string, fromJs: string => option<'variadicType>) => Morphism(
    String,
    usingString(i => {
      switch i->fromJs {
      | Some(internalValue) => internalValue
      | None => raise(TypeError(`This String("${i}") not a valid value here. Hint: ${hint}`))
      }
    }),
  )

  let rec toString = (type_: t) =>
    switch type_ {
    | Any => "Any"
    | String => "String"
    | Literal(lit) => `Literal: ${lit}`
    | Int => "Integer"
    | Float => "Float"
    | Boolean => "Boolean"
    | Datetime
    | Date => "Date"
    | Self => "Self (recursive)"
    | Collection(m) => {
        module M = unpack(m: Deserializer)
        "Collection of " ++ M.name
      }

    | Array(t) => "Array of " ++ t->toString
    | Tuple(bases) => `Tuple of (${bases->Array.map(toString)->Array.join(", ")})`
    | Object(fields) => {
        let desc = fields->Array.map(((field, t)) => `${field}: ${t->toString}`)->Array.join(", ")
        `Object of {${desc}}`
      }

    | OptionalWithDefault(t, _)
    | Optional(t) =>
      "Null of " ++ t->toString
    | Mapping(t) => `Mapping of ${t->toString}`
    | Morphism(t, _) => t->toString ++ " to apply a morphism"
    | Deserializer(m) => {
        module M = unpack(m: Deserializer)
        M.name
      }

    | DefaultWhenInvalid(t, _) => `Protected ${t->toString}`
    }

  let _taggedToString = (tagged: Js.Json.tagged_t) => {
    switch tagged {
    | Js.Json.JSONFalse => "Boolean(false)"
    | Js.Json.JSONTrue => "Boolean(true)"
    | Js.Json.JSONNull => "Null"
    | Js.Json.JSONString(text) => `String("${text}")`
    | Js.Json.JSONNumber(number) => `Number(${number->Float.toString})`
    | Js.Json.JSONObject(obj) => `Object(${obj->Js.Json.stringifyAny->Option.getOr("...")})`
    | Js.Json.JSONArray(array) => `Array(${array->Js.Json.stringifyAny->Option.getOr("...")})`
    }
  }

  let rec extractValue = (
    values: Dict.t<Js.Json.t>,
    field: string,
    shape: t,
    self: t,
  ): FieldValue.t => {
    switch values->Dict.get(field) {
    | Some(value) => value->fromUntagged(shape, self)
    | None =>
      switch shape {
      | DefaultWhenInvalid(_, _) => Js.Json.null->fromUntagged(shape, self)
      | Optional(_) => Js.Json.null->fromUntagged(shape, self)
      | OptionalWithDefault(_, default) => default
      | _ => raise(TypeError(`Missing non-optional field '${field}'`))
      }
    }
  }
  and fromUntagged = (untagged: Js.Json.t, shape: t, self: t): FieldValue.t => {
    switch (shape, untagged->Js.Json.classify) {
    | (Any, _) => untagged->FieldValue.any
    | (Literal(expected), Js.Json.JSONString(text)) =>
      if text == expected {
        FieldValue.string(text)
      } else {
        raise(TypeError(`Expecting literal ${expected}, got ${text}`))
      }
    | (String, Js.Json.JSONString(text)) => FieldValue.string(text)
    | (Int, Js.Json.JSONNumber(number)) => FieldValue.int(number->Float.toInt)
    | (Float, Js.Json.JSONNumber(number)) => FieldValue.float(number)
    | (Boolean, Js.Json.JSONTrue) => FieldValue.boolean(true)
    | (Boolean, Js.Json.JSONFalse) => FieldValue.boolean(false)
    | (Tuple(bases), Js.Json.JSONArray(items)) => {
        let lenbases = bases->Array.length
        let lenitems = items->Array.length
        if lenbases == lenitems {
          let values = Belt.Array.zipBy(items, bases, (i, b) => fromUntagged(i, b, self))
          values->FieldValue.array
        } else {
          raise(
            TypeError(`Expecting ${lenbases->Int.toString} items, got ${lenitems->Int.toString}`),
          )
        }
      }

    | (Datetime, Js.Json.JSONString(s))
    | (Date, Js.Json.JSONString(s)) => {
        let r = Js.Date.fromString(s)
        if r->Js.Date.getDate->Js.Float.isNaN {
          raise(TypeError(`Invalid date ${s}`))
        }
        r->FieldValue.any
      }

    | (Datetime, Js.Json.JSONNumber(f))
    | (Date, Js.Json.JSONNumber(f)) => {
        let r = Js.Date.fromFloat(f)
        if r->Js.Date.getDate->Js.Float.isNaN {
          raise(TypeError(`Invalid date ${f->Js.Float.toString}`))
        }
        r->FieldValue.any
      }

    | (Array(shape), Js.Json.JSONArray(items)) =>
      FieldValue.array(items->Array.map(item => item->fromUntagged(shape, self)))
    | (Mapping(f), Js.Json.JSONObject(values)) =>
      values->Dict.mapValues(v => v->fromUntagged(f, self))->FieldValue.mapping
    | (Object(fields), Js.Json.JSONObject(values)) =>
      FieldValue.object(
        fields
        ->Array.map(((field, shape)) => {
          let value = switch extractValue(values, field, shape, self) {
          | value => value
          | exception TypeError(msg) => raise(TypeError(`Field "${field}": ${msg}`))
          }
          (field, value)
        })
        ->Dict.fromArray,
      )

    | (OptionalWithDefault(_, value), Js.Json.JSONNull) => value
    | (OptionalWithDefault(shape, _), _) => untagged->fromUntagged(shape, self)
    | (Optional(_), Js.Json.JSONNull) => FieldValue.null
    | (Optional(shape), _) => untagged->fromUntagged(shape, self)
    | (Morphism(shape, f), _) => untagged->fromUntagged(shape, self)->f->FieldValue.any

    | (Collection(m), Js.Json.JSONArray(items)) => {
        module M = unpack(m: Deserializer)
        items
        ->Array.map(M.fromJSON)
        ->Array.filter(x => x->Result.isOk)
        ->Array.map(FieldValue.any)
        ->FieldValue.array
      }

    | (Deserializer(m), _) => {
        module M = unpack(m: Deserializer)
        switch untagged->M.fromJSON {
        | Ok(res) => res->FieldValue.any
        | Error(msg) => raise(TypeError(msg))
        }
      }

    | (DefaultWhenInvalid(t, default), _) =>
      switch untagged->fromUntagged(t, self) {
      | res => res
      | exception TypeError(msg) => {
          Js.Console.warn2("Detected and ignore (with default): ", msg)
          default
        }
      }
    | (Self, _) => untagged->fromUntagged(self, self)
    | (expected, actual) =>
      raise(TypeError(`Expected ${expected->toString}, but got ${actual->_taggedToString} instead`))
    }
  }

  let rec checkFieldsSanity = (name: string, fields: t, optional: bool): result<unit, string> =>
    switch (fields, optional) {
    | (Self, false) => Error(`${name}: Trivial infinite recursion 'let fields = Self'`)
    | (Self, true) => Ok()

    | (Any, _) => Ok()
    | (String, _) | (Float, _) | (Int, _) | (Literal(_), _) => Ok()
    | (Boolean, _) | (Date, _) | (Datetime, _) => Ok()
    | (Morphism(_, _), _) => Ok()

    | (Collection(mod), _)
    | (Deserializer(mod), _) => {
        module M = unpack(mod: Deserializer)
        switch M.checkFieldsSanity() {
        | Ok() => Ok()
        | Error(msg) => Error(`${name}/ ${msg}`)
        }
      }

    | (DefaultWhenInvalid(fields, _), _)
    | (OptionalWithDefault(fields, _), _)
    | (Optional(fields), _) =>
      checkFieldsSanity(name, fields, true)

    | (Object(fields), optional) =>
      fields
      ->Array.map(((fieldName, field)) => () =>
        checkFieldsSanity(`${name}::${fieldName}`, field, optional))
      ->Array.reduce(Ok([]), (res, nextitem) =>
        res->Result.flatMap(arr => nextitem()->Result.map(i => arr->Array.concat([i])))
      )
      ->Result.map(_ => ())

    /// Mappings and arrays can be empty, so their payloads are
    /// automatically optional.
    | (Mapping(field), _) | (Array(field), _) => checkFieldsSanity(name, field, true)

    | (Tuple(fields), optional) =>
      fields
      ->Array.mapWithIndex((field, index) => () =>
        checkFieldsSanity(`${name}[${index->Int.toString}]`, field, optional))
      ->Array.reduce(Ok([]), (res, nextitem) =>
        res->Result.flatMap(arr => nextitem()->Result.map(i => arr->Array.concat([i])))
      )
      ->Result.map(_ => ())
    }
}

module type Serializable = {
  type t
  let fields: Field.t
}

module MakeDeserializer = (S: Serializable): (Deserializer with type t = S.t) => {
  type t = S.t
  let fields = S.fields
  %%private(let (loc, _f) = __LOC_OF__(module(S: Serializable)))
  let name = `Deserializer ${__MODULE__}, ${loc}`

  %%private(external _toNativeType: FieldValue.t => t = "%identity")

  @doc("Checks for trivial infinite-recursion in the fields of the module.

  Notice this algorithm is just an heuristic, and it might happen that are
  cases of infinite-recursion not detected and cases where detection is a
  false positive.

  You should use this only while debugging/developing to verify your data.

  ")
  let checkFieldsSanity = () => Field.checkFieldsSanity(name, fields, false)

  @doc("Parse a `Js.Json.t` into `result<t, string>`")
  let fromJSON = (json: Js.Json.t): result<t, _> => {
    switch json->Field.fromUntagged(fields, fields) {
    | res => Ok(res->_toNativeType)
    | exception TypeError(e) => Error(e)
    }
  }
}
