;;; Directory Local Variables            -*- no-byte-compile: t -*-
;;; For more information see (info "(emacs) Directory Variables")

((nil . ((projectile-project-compilation-cmd . "make compile")))
  (js-json-mode . ((tab-width . 2)
                    (js-indent-level . 2)))
  (rescript-mode . ((projectile-project-compilation-cmd . "make compile")
                     (indent-tabs-mode . nil))))
